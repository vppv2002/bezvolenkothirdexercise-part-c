package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String searchMe = "tree";
        System.out.println("Введите количество строк: ");
        int kolStrok = new Scanner(System.in).nextInt();
        String[] arr = new String[kolStrok];
        for (int i = 0; i < kolStrok; i++) {
            System.out.println("Введите " + (i + 1) + "ю строку");
            arr[i] = new Scanner(System.in).nextLine();
        }
        for (int j = 0; j < kolStrok; j++) {
            if (arr[j].contains(searchMe)) {
                System.out.println("Эта строка содержит ключевое слов: " + arr[j]);
            }
        }
    }
}